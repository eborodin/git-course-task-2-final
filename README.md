# Удаление ненужных веток
Написать bash который будет удалять все ветки что уже есть в ветке мастер. [Пример с лекции](https://github.com/MaksymSemenykhin/git-course-example-1.3) 


Удалять все ветки кроме master, dev, release


# Expected result:
- Публичный репозиторий
- Склоненный с репозитория https://github.com/MaksymSemenykhin/git-course-task/tree/task%232
- Скрипта  для работы на ubuntu сервере
- Процедура будет работать с прод репой.
- В runMe.sh сам скрипт
- В README файле описано что именно делает скрипт и другие детали

# The task plan:
1. Prepare conditions for the task actions
2. Run script


# Script:
1 cd ~/git/git-course-task-2-final
2 for branch in $(git branch -r --merged master | grep prod | grep -v dev | grep -v release | grep -v master)
3 do
4  git push prod --delete "${branch#*/}"
5 done

1 - переходим в рабочую область
2 - в цикле ищем смержденные в мастер prod репозитории ветки, исключая ветки dev, release, master
4 - удаляем найденные по фильтру ветки в ремоут репозитории prod


# 1. Prepare task conditions

git remote add prod teamcity@localhost:/srv/git/devops.git
git fetch prod
git co -b master
git pull 

git co -b dev
touch test.dev
git add tets.dev
git ci -m "added test.dev"
git push prod

git co -b release
git add test.rel
git ci -m "added test.rel"
git push prod

git co -b task#1
git pull origin task#1
git push prod

git co -b task#3
git pull origin task#1
git push prod

git co -b task#4
git pull origin task#1
git push prod

git co master
git merge dev
git merge release
git merge task#1
git merge task#2
git merge task#3
git merge task#4
git push prod
git co -b dev
git push prod
git co -b release
git push prod
git co -b task#1
git push prod
git co -b task#2
git push prod
git co -b task#3
git push prod
git co -b task#4
git push prod

# Pre-processing Results:
[teamcity@ip-172-31-23-181] ~/git/git-course-task-2-final (master) $ git br -a
  dev
* master
  release
  task#1
  task#2
  task#3
  task#4
  remotes/origin/HEAD -> origin/task#2
  remotes/origin/master
  remotes/origin/task#1
  remotes/origin/task#2
  remotes/origin/task#3
  remotes/origin/task#4
  remotes/prod/dev
  remotes/prod/master
  remotes/prod/release
  remotes/prod/task#1
  remotes/prod/task#2
  remotes/prod/task#3
  remotes/prod/task#4

# Run runMe.sh

[teamcity@ip-172-31-23-181] ~/git $

./runMe.sh
To localhost:/srv/git/devops.git
 - [deleted]         task#1
To localhost:/srv/git/devops.git
 - [deleted]         task#2
To localhost:/srv/git/devops.git
 - [deleted]         task#3
To localhost:/srv/git/devops.git
 - [deleted]         task#4

# Post-processing results:
  dev
* master
  release
  task#1
  task#2
  task#3
  task#4
  remotes/origin/HEAD -> origin/task#2
  remotes/origin/master
  remotes/origin/task#1
  remotes/origin/task#2
  remotes/origin/task#3
  remotes/origin/task#4
  remotes/prod/dev
  remotes/prod/master
  remotes/prod/release
