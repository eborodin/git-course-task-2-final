#!/bin/bash
cd ~/git/git-course-task-2-final
for branch in $(git branch -r --merged master | grep prod | grep -v dev | grep -v release | grep -v master)
do
  git push prod --delete "${branch#*/}"
done